#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <float.h>
#include <limits.h>

int randomInteger(int low, int high);

int seed = 1;

/*void main(void)
{
	//  working variables
	int i = 0;
	int n = 0;


	// Print hex digits one after another
	for(i=0; i<10; i++)
	{
      	n = randomInteger(0,10)%3;
		printf("the number is: %d\n", n);
	}

}*/


int randomInteger(int low, int high)
{
  double randNum = 0.0;
  int multiplier = 2743;
  int addOn = 5923;
  double max = INT_MAX + 1.0;

  if (low > high)
    return randomInteger(high, low);
  else
  {
    seed = seed*multiplier + addOn;
    randNum = seed;

    if (randNum <0)
      randNum = randNum + max;

    randNum = randNum/max;

    //return high;
    return ((int)((high-low+1)*randNum))+low;
   }
}


