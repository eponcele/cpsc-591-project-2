#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "driverlib/debug.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/pwm.h"
#include "drivers/rit128x96x4.h"
#include "taskInfo.h"
#include "structs.h"
#include <stdio.h>
#include "inc/lm3s8962.h"

//Global variables
unsigned char north, east, west;
Bool trainPresent, gridlock;
unsigned int trainSize, traversalTime;
unsigned int counter;
unsigned int gridlockDelay;
unsigned int traversalTimeDelay;
unsigned int northTrainFlash;
Bool northTrainFlashOn;
unsigned int eastTrainFlash;
Bool eastTrainFlashOn;
unsigned int westTrainFlash;
Bool westTrainFlashOn;

//Task Data
trainData northTrainData;
trainData eastTrainData;
trainData westTrainData;
trainComStruct trainComData;
switchData switchControlData;
displayData oledDisplayData;

//TCB
TCB northTrain;
TCB eastTrain;
TCB westTrain;
TCB switchControl;
TCB trainCom;
TCB oledDisplay;

void initializeVariables() 
{
  north = east = west = '0';
  trainPresent = FALSE;
  trainSize = traversalTime = 0;
  gridlock = FALSE;
  
  gridlockDelay = traversalTimeDelay = 0;
  northTrainFlash = eastTrainFlash = westTrainFlash = 0;
  northTrainFlashOn = eastTrainFlashOn = westTrainFlashOn = FALSE;
}

void initializeTaskData()
{
  //north train data initialization
  northTrainData.direction = &north;
  northTrainData.trainPresent = &trainPresent;
  
  //east train data initialization
  eastTrainData.direction = &east;
  eastTrainData.trainPresent = &trainPresent;
  
  //west train data initialization  
  westTrainData.direction = &west;
  westTrainData.trainPresent = &trainPresent;
  
  //train communication data initialization
  trainComData.north = &north;
  trainComData.east = &east;
  trainComData.west = &west;
  trainComData.trainPresent = &trainPresent;
  trainComData.trainSize = &trainSize;
  
  //switch control data initialization
  switchControlData.north = &north;
  switchControlData.east = &east;
  switchControlData.west = &west;
  switchControlData.trainPresent = &trainPresent;
  switchControlData.trainSize = &trainSize;
  switchControlData.traversalTime = &traversalTime;
  switchControlData.gridlock = &gridlock;
  
  //oled display data initialization
  oledDisplayData.north = &north;
  oledDisplayData.east = &east;
  oledDisplayData.west = &west;
  oledDisplayData.trainPresent = &trainPresent;
  oledDisplayData.trainSize = &trainSize;
  oledDisplayData.traversalTime = &traversalTime;
}

void initializeTaskControlBlocks()
{
  void* convertToVoidPtr;
  
  //north train task control block initialization
  convertToVoidPtr = &northTrainData;
  northTrain.taskDataPtr = convertToVoidPtr;
  northTrain.myTask = northTrainFcn;

  //east train task control block initialization
  convertToVoidPtr = &eastTrainData;
  eastTrain.taskDataPtr = convertToVoidPtr;
  eastTrain.myTask = eastTrainFcn;
  
  //west train task control block initialization
  convertToVoidPtr = &westTrainData;
  westTrain.taskDataPtr = convertToVoidPtr;
  westTrain.myTask = westTrainFcn;
  
  //switch control task control block initialization  
  convertToVoidPtr = &switchControlData;
  switchControl.taskDataPtr =  convertToVoidPtr;
  switchControl.myTask = switchControlFcn;  
  
  //train communication task control block initialization
  convertToVoidPtr = &trainComData;
  trainCom.taskDataPtr = convertToVoidPtr;
  trainCom.myTask = trainComFcn;
  
  //oled display task control block initialization
  convertToVoidPtr = &oledDisplayData;  
  oledDisplay.taskDataPtr = convertToVoidPtr;
  oledDisplay.myTask = oledDisplayFcn;
}

void delay(int aDelay)
{
    volatile unsigned long i = 0;

    volatile unsigned int j = 0;
    
    for (i = aDelay; i > 0; i--)
    {
        for (j = 0; j < 100; j++);
    }

    return;
}

void initializeBoard() 
{ 
  
  //##########Initialize Flashing############
  // Enable the GPIO port that is used for the on-board LED.
  SYSCTL_RCGC2_R = SYSCTL_RCGC2_GPIOF;

  // Do a dummy read to insert a few cycles after enabling the peripheral.
  //ulLoop = SYSCTL_RCGC2_R;

  // Enable the GPIO pin for the LED (PF0).  Set the direction as output, and
  // enable the GPIO pin for digital function.
  GPIO_PORTF_DIR_R = 0x01;
  GPIO_PORTF_DEN_R = 0x01;
  
  //Turn LED off
  GPIO_PORTF_DATA_R &= ~(0x01); 
  
  //########Initialize PWM for Beeping##############
  unsigned long ulPeriod; 
  SysCtlPWMClockSet(SYSCTL_PWMDIV_1);
  
  
  // Set the clocking to run directly from the crystal.
  SysCtlClockSet(SYSCTL_SYSDIV_1 | SYSCTL_USE_OSC | SYSCTL_OSC_MAIN |
                   SYSCTL_XTAL_8MHZ);
  
  // Enable the PWM and GPIO used to control it
  //
  SysCtlPeripheralEnable(SYSCTL_PERIPH_PWM);
  SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
  SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOG);
  
  // Set GPIO F0 and G1 as PWM pins in order to output the PWM0 and
  // PWM1 signals.
  //
  GPIOPinTypePWM(GPIO_PORTF_BASE, GPIO_PIN_0);
  GPIOPinTypePWM(GPIO_PORTG_BASE, GPIO_PIN_1);
  
  //
  // Compute the PWM period based on the system clock.
  //
  ulPeriod = SysCtlClockGet() / 440;

  //
  // Set the PWM period to 440 (A) Hz.
  //
  PWMGenConfigure(PWM_BASE, PWM_GEN_0,
                  PWM_GEN_MODE_UP_DOWN | PWM_GEN_MODE_NO_SYNC);
  PWMGenPeriodSet(PWM_BASE, PWM_GEN_0, ulPeriod);

  //
  // Set PWM0 to a duty cycle of 25% and PWM1 to a duty cycle of 75%.
  //
  PWMPulseWidthSet(PWM_BASE, PWM_OUT_0, ulPeriod / 4);
  PWMPulseWidthSet(PWM_BASE, PWM_OUT_1, ulPeriod * 3 / 4);

  //
  // Enable the PWM0 and PWM1 output signals.
  //
  PWMOutputState(PWM_BASE, PWM_OUT_0_BIT | PWM_OUT_1_BIT, true);

  //#########Initialize the OLED display#################
  RIT128x96x4Init(1000000);
}

int main()
{
  //initialize all members
  initializeVariables();
  initializeTaskData();
  initializeTaskControlBlocks();
  initializeBoard();
    
  //initialize queue
  TCB queue[7];
  queue[0] = trainCom;
  queue[1] = northTrain;
  queue[2] = eastTrain;
  queue[3] = westTrain;
  queue[4] = switchControl;
  queue[5] = oledDisplay;
  
  //used to convert the structs to void pointers
//  void* convertToVoidPtr;
  
//AudioPlaySound(g_pusPlayerEffect, sizeof(g_pusPlayerEffect) / 2);
  
  while (1)
  {
    delay(700);
    PWMGenEnable(PWM_BASE, PWM_GEN_0);
    
    delay(700);
    PWMGenDisable(PWM_BASE, PWM_GEN_0);
   
    for (int i = 0; i < 6; i++) 
    {
      //convertToVoidPtr = queue[i].taskDataPtr;
      queue[i].myTask(queue[i].taskDataPtr);
    }
    /*
    convertToVoidPtr = &trainCom.taskDataPtr;
    trainCom.myTask(trainCom.taskDataPtr);
    
    convertToVoidPtr = &switchControlData;
    switchControl.myTask(convertToVoidPtr);
    switchControl.myTask(convertToVoidPtr);
  
    convertToVoidPtr = &northTrainData;
    northTrain.myTask(convertToVoidPtr);
    
    convertToVoidPtr = &eastTrainData;
    eastTrain.myTask(convertToVoidPtr);
    
    convertToVoidPtr = &oledDisplayData;
    oledDisplay.myTask(convertToVoidPtr);
    */
    delay(700);
    counter++;
  }
  
  return 0;
}
